# BEMAKER.EU static convertion

### dev env setup:

<code>
$ virtualenv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
</code>

to deactivate venv, simply type: <code>deactivate</code> at prompt

### pelican structure:

* content in folder content
* theme in folder theme

### running sass:

<code>sass --watch input.scss output.css</code>
